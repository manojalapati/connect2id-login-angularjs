package com.nimbusds.login.rest.errors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Alex Bogdanovski [alex@erudika.com]
 */
@Controller
public class RedirectingErrorController implements ErrorController {

	private static final String PATH = "/error";

	@RequestMapping(value = PATH)
	public String error(HttpServletRequest req, HttpServletResponse res) {
		return "redirect:/err";
	}

	@Override
	public String getErrorPath() {
		return PATH;
	}
}
