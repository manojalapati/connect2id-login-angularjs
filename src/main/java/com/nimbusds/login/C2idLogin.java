package com.nimbusds.login;

import com.nimbusds.login.config.ApplicationProperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import java.net.UnknownHostException;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.ErrorPageRegistrar;
import org.springframework.boot.web.server.ErrorPageRegistry;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;

@SpringBootApplication
@EnableConfigurationProperties({ApplicationProperties.class})
public class C2idLogin extends SpringBootServletInitializer {

	public C2idLogin() {
	}

	@Bean
	public ErrorPageRegistrar errorPageRegistrar() {
		return (ErrorPageRegistry epr) -> {
			epr.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/index.html"));
		};
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(C2idLogin.class);
	}

	/**
	 * Main method, used to run the application.
	 *
	 * @param args the command line arguments
	 * @throws UnknownHostException if the local host name could not be resolved into an address
	 */
	public static void main(String[] args) throws UnknownHostException {
		SpringApplication app = new SpringApplication(C2idLogin.class);
		app.setWebApplicationType(WebApplicationType.SERVLET);
		app.run(args);
	}
}
