package com.nimbusds.login.config;

import com.nimbusds.login.spi.UserAuthenticator;
import com.nimbusds.login.spi.impl.LdapUserAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import java.io.File;
import java.nio.file.Paths;
import java.util.*;
import javax.servlet.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.server.MimeMappings;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;

/**
 * Configuration of web application with Servlet 3.0 APIs.
 */
@Configuration
public class WebConfigurator implements ServletContextInitializer,
		WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {

	private final Logger logger = LoggerFactory.getLogger(WebConfigurator.class);
	private final Environment env;

	public WebConfigurator(Environment env) {
		this.env = env;
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		if (env.getActiveProfiles().length != 0) {
			logger.debug("Web application configuration, using profiles: {}", (Object[]) env.getActiveProfiles());
		}
	}

	/**
	 * Customize the Servlet engine: Mime types, the document root, the cache.
	 */
	@Override
	public void customize(ConfigurableServletWebServerFactory container) {
		MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
		// IE issue
		mappings.add("html", "text/html;charset=utf-8");
		container.setMimeMappings(mappings);
		// When running in an IDE or with ./mvnw spring-boot:run, set location of the static web assets.
		setLocationForStaticAssets(container);
	}

	private void setLocationForStaticAssets(ConfigurableServletWebServerFactory container) {
		File root;
		String prefixPath = resolvePathPrefix();
		if (env.acceptsProfiles("prod")) {
			root = new File(prefixPath + "target/www/");
		} else {
			root = new File(prefixPath + "src/main/webapp/");
		}
		if (root.exists() && root.isDirectory()) {
			container.setDocumentRoot(root);
		}
	}

	/**
	 * Resolve path prefix to static resources.
	 */
	private String resolvePathPrefix() {
		String fullExecutablePath = this.getClass().getResource("").getPath();
		String rootPath = Paths.get(".").toUri().normalize().getPath();
		String extractedPath = fullExecutablePath.replace(rootPath, "");
		int extractionEndIndex = extractedPath.indexOf("target/");
		if (extractionEndIndex <= 0) {
			return "";
		}
		return extractedPath.substring(0, extractionEndIndex);
	}

	@Bean
	public UserAuthenticator userAuthenticator(ApplicationProperties config) {
		String authenticatorClass = config.getAuthenticatorClass();
		if (!StringUtils.isBlank(authenticatorClass)) {
			for (UserAuthenticator authImpl : ServiceLoader.load(UserAuthenticator.class)) {
				if (authImpl != null && authenticatorClass.equalsIgnoreCase(authImpl.getClass().getSimpleName())) {
					return authImpl;
				}
			}
		}
		return new LdapUserAuthenticator(config);
	}
}
