(function() {
    'use strict';

    angular
        .module('C2idLoginApp')
        .controller('ErrorController', ErrorController);

    ErrorController.$inject = ['$scope', '$routeParams', 'Base64'];

    function ErrorController ($scope, $routeParams, Base64) {
		$scope.errorMessage = Base64.decode($routeParams.msg);
    }
})();
