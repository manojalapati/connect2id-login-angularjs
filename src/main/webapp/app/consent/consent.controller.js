(function() {
    'use strict';

    angular
        .module('C2idLoginApp')
        .controller('ConsentController', ConsentController);

    ConsentController.$inject = ['$rootScope', '$scope', '$location', '$cookies', 'Auth'];

    function ConsentController ($rootScope, $scope, $location, $cookies, Auth) {

		$scope.addRemoveScopes = function (scope, isChecked) {
			if (isChecked) {
				$rootScope.consentedScopes.push(scope);
			} else {
				$rootScope.consentedScopes = $rootScope.consentedScopes.filter(function (value) {
					return value !== scope;
				});
			}
		};

		$scope.addRemoveClaims = function (claim, isChecked) {
			if (isChecked) {
				$rootScope.consentedClaims.push(claim);
			} else {
				$rootScope.consentedClaims = $rootScope.consentedClaims.filter(function (value) {
					return value !== claim;
				});
			}
		};

		$scope.submitConsent = function () {
			Auth.updateAuthRequest({
				authSessionId: $location.search().authSessionId
			}, {
				scope: $rootScope.consentedScopes,
				claims: $rootScope.consentedClaims
			}, function (c2idResponse) {
				$rootScope.$broadcast('event:switch-action', c2idResponse);
			}, function (err) {
				$rootScope.$broadcast('event:auth-error', err.data.message);
			});
		};

		$scope.denyAuthorization = function () {
			Auth.cancelAuthRequest({
				authSessionId: $location.search().authSessionId
			}, {}, function (c2idResponse) {
				$rootScope.$broadcast('event:switch-action', c2idResponse);
			}, function (err) {
				$rootScope.$broadcast('event:auth-error', err.data.message);
			});
		};

		$scope.logoutSubject = function () {
			Auth.logoutSubject(function () {
				$rootScope.authenticatedUser = null;
				$scope.authenticationError = false;
				var qs = $location.absUrl();
				Auth.initAuthRequest({
					qs: qs.substring(qs.indexOf("?") + 1)
				}, function (c2idResponse) {
					$rootScope.$broadcast('event:switch-action', c2idResponse);
				}, function (err) {
					$rootScope.$broadcast('event:auth-error', err.data.message);
				});
			}, function (err) {
				console.error(err.data);
			});
		};
    }
})();
