(function() {
    'use strict';

    angular
        .module('C2idLoginApp')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$rootScope', '$scope', '$location', 'Auth'];

    function LoginController ($rootScope, $scope, $location, Auth) {
		$scope.fieldErrors = {};
        $scope.authenticationError = false;

		var handleLdapAuthHttpError = function (err) {
			if (err.data && err.data.fieldErrors) {
				angular.forEach(err.data.fieldErrors, function (val) {
					$scope.fieldErrors[val.field] = "error." + val.message;
				});
			}
			$scope.authenticationError = true;
		};

        $scope.login = function (valid) {
			if (valid) {
				var authSessionId = $location.search().authSessionId;
				if (!authSessionId) {
					$rootScope.$broadcast('event:auth-error', 'The authorisation session identifier is missing.');
				} else {
					Auth.authenticateSubject({
						authSessionId: authSessionId
					}, {
						username: $scope.username,
						password: $scope.password
					}).$promise.then(function (c2idResponse) {
						$scope.authenticationError = false;
						$rootScope.authenticatedUser = c2idResponse.data;
						$rootScope.metadata.authenticatedUser = c2idResponse.data;
						$rootScope.$broadcast('event:switch-action', c2idResponse);
					}).catch(function (err) {
						handleLdapAuthHttpError(err);
					});
				}
			} else {
				handleLdapAuthHttpError({});
			}
        };

        $scope.cancel = function () {
            $scope.authenticationError = false;
			var exit = $location.search().redirect_uri;
			var delim = exit.indexOf("?") < 0 ? "?" : "&";
			window.location.href = exit + delim + "state=" + $location.search().state + "&error=access_denied" +
					"&error_description=The user canceled the authentication.";
        };
    }
})();
