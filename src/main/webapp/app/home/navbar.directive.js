(function() {
    'use strict';

    angular
        .module('C2idLoginApp')
        .directive('navbar', Navbar);

    function Navbar () {
		return {
			restrict: 'AE',
			templateUrl: 'app/home/navbar.html'
		};
    }
})();
