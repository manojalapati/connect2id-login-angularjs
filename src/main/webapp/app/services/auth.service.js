(function() {
    'use strict';

    angular
        .module('C2idLoginApp')
        .factory('Auth', Auth);

    Auth.$inject = ['$resource'];

    function Auth ($resource) {
		return $resource('api', {}, {
			'pingServer': {
				url: 'api/meta',
				method: 'GET'
			},
			'initAuthRequest': {
				url: 'api/initAuthRequest',
				method: 'POST'
			},
			'authenticateSubject': {
				url: 'api/authenticateSubject',
				method: 'POST'
			},
			'verifySubject': {
				url: 'api/verifySubject',
				method: 'GET'
			},
			'logoutSession': {
				url: 'api/logoutSession',
				method: 'POST'
			},
			'logoutSessionConfirm': {
				url: 'api/logoutSessionConfirm',
				method: 'PUT'
			},
			'updateAuthRequest': {
				url: 'api/updateAuthRequest/:authSessionId',
				method: 'PUT'
			},
			'cancelAuthRequest': {
				url: 'api/cancelAuthRequest/:authSessionId',
				method: 'DELETE'
			},
			'logoutSubject': {
				url: 'api/logoutSubject/:sid',
				method: 'DELETE'
			}
		});
    }
})();
