(function() {
    'use strict';

    angular
        .module('C2idLoginApp')
        .config(localStorageConfig);

    localStorageConfig.$inject = ['$localStorageProvider', '$sessionStorageProvider'];

    function localStorageConfig($localStorageProvider, $sessionStorageProvider) {
        $localStorageProvider.setKeyPrefix('c2id-');
        $sessionStorageProvider.setKeyPrefix('c2id-');
    }
})();
