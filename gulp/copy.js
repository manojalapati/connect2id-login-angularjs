'use strict';

var gulp = require('gulp'),
    rev = require('gulp-rev'),
	gulpIf = require('gulp-if'),
    plumber = require('gulp-plumber'),
    es = require('event-stream'),
    flatten = require('gulp-flatten'),
    replace = require('gulp-replace'),
    bowerFiles = require('main-bower-files'),
    changed = require('gulp-changed');

var handleErrors = require('./handle-errors');
var config = require('./config');

module.exports = {
    i18n: i18n,
    languages: languages,
    common: common,
    images: images
};

var yorc = require('../.yo-rc.json')['generator-jhipster'];

function i18n() {
    return gulp.src(config.app + 'i18n/**')
        .pipe(plumber({errorHandler: handleErrors}))
        .pipe(changed(config.dist + 'i18n/'))
        .pipe(gulp.dest(config.dist + 'i18n/'));
}

function languages() {
    var locales = yorc.languages.map(function (locale) {
        return config.bower + 'angular-i18n/angular-locale_' + locale + '.js';
    });
    return gulp.src(locales)
        .pipe(plumber({errorHandler: handleErrors}))
        .pipe(changed(config.app + 'i18n/'))
        .pipe(gulp.dest(config.app + 'i18n/'));
}

function common() {
    return gulp.src([config.app + 'robots.txt', config.app + 'favicon.ico'], { dot: true })
        .pipe(plumber({errorHandler: handleErrors}))
        .pipe(changed(config.dist))
        .pipe(gulp.dest(config.dist));
}

function images(cb) {
	var files = bowerFiles({filter: ['**/*.{gif,jpg,png}']});
	if (files.length > 0) {
		return gulp.src(files, { base: config.bower })
			.pipe(plumber({errorHandler: handleErrors}))
			.pipe(changed(config.dist +  'bower_components'))
			.pipe(gulp.dest(config.dist +  'bower_components'));
	} else {
		cb();
	}
}
